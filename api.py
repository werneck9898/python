import json
import requests

class galaxAPI:
    #Abaixo temos o método construtor para a classe API que pode receber dois parâmetros, baseURL e headers.
    
    def __init__(self, baseURL = None, headers = None):
        self.headers = headers
        self.baseURL = baseURL
    
    #Método para criar uma instância de API caso ela tenha sido inicializada sem nenhum valor para baseURl e headers
    def create(url: str, auth: json):
        return galaxAPI(url, auth)
    
    #Métodos para as requisições GET e POST usando a biblioteca Requests
    def get(self, url: str) -> json: 
        return requests.get(self.baseURL + url, headers=self.headers).json()
    
    def post() -> int:
        status_code = 0                     #Tente implementar uma função que irá fazer uma requisição POST 
                                            #utilizando a biblioteca requests. Você pode utilizar a função get() acima 
                                            #como exemplo. 
                                            #Quais parâmetros a função post() deve receber?
                                            #Qual valor deve ser atribuido a status_code?
        return status_code
   