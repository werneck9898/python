<h2>Nesse treinamento você ira aprender a fazer requisições HTTP e manipular protocolos de email em Python. Para começar vamos precisar fazer algumas checagens.</h2>

1. Checar e instalar dependências
	
	Iremos usar duas bibliotecas nativas de Python nesse treinamento: `SMTP` e `Requests`. Verifique se python3 está instalado na sua máquina e instale as dependẽncias se necessário

	```
	python3 --version

	sudo apt-get update

	sudo apt install python3-pip

	pip3 install requests

	```

2. Agora clone a pasta treinamento


3. Dentro da pasta você vai encontrar trẽs arquivos: `main.py`, `api.py`, `smtp.py`. No arquivo main você vai encontrar um exemplo de envio de email por SMTP e outro de requisições HTTP em python. Para que o main.py possa ser executado você primeiro deve configurar corretamente as váriaveis para autenticação do email e das requisções HTTP.

```
Nas linhas 6 e 7 do main.py voce deve fornencer um usuario e senha gmail válidos para enviar o email e também endereços de emails válidos para enviar os emails de teste

Nas linhas 14, 16 e 17 voce deve fornecer uma TOKEN (KEY: VALUE), uma baseURl e um caminho GET válidos para fazer a requisição
```

4. Para usar o SMTP com o GMAIL voce deve permitir o Acesso de Apps menos Seguros em https://myaccount.google.com/lesssecureapps

5. Para executar um script python utilizamos a seguinte sintaxe:

```
python3 meuscript.py

ou

python meuscript.py
```
