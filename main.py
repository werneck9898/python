from smtp import Email
from api import galaxAPI


#Abaixo temos dois exemplo: Envio de Emails SMTP e Requisições HTTP em Python.
#Antes de começar vá até os arquivos "smtp.py" e depois "api.py" para entender a definição da classe, atributos e métodos.


###########################################################################################################################

#SMTP e Envio de Emails
smtp = Email(email="your_email", password="your_password") #Criando um objeto Email
smtp.setEmailInfo(to_addrs=['email@gmail.com', "second@hotmail.com"], subject="Teste", message="Olá, boa tarde")
#Observe o Método setEmailInfo. Em Python, passar os parãmetros para um método utilizando os nomes dos parâmetros é opcional.

#Exemplo: smtp.setEmailInfo(['email@gmail.com', "second@hotmail.com"], "Teste", "Olá, boa tarde")
          #smtp.setEmailInfo(['email@gmail.com', "second@hotmail.com"], "Teste",  message="Olá, boa tarde")

print("O email do remetente é " + smtp.getEmail())

#Para enviar um email remova o comentário da linha abaixo
#smtp.sendEmail()

#Sintaxe para receber input do usuário. Também podemos utilizar o raw_input ou getpass(Senhas)
input("Leia o exemplo de requests e pressione Enter para continuar para o exemplo de Requests") 


###########################################################################################################################


#Requesições HTTP e conectando na API

TOKEN = {"D5B2CC728ECD8BEEB2E109ECEF91EF2F": "3969FC519C8F884C6038E48DCEECC340"} #TOKEN de Autorização (POSTMAN)

api = galaxAPI.create(url="http://192.168.55.13/", auth=TOKEN)  #Criando uma instancia de API utilizando o método create

posts: dict = api.get("help/posts")  #Buscando os dados de Help/Posts, posts deve ser do tipo dictionary

#Como o caminho Help/Posts vai retornar um JSON do tipo { Posts: [{post1}, {post2}...] } o Python vai interpretar esse JSON 
# como o tipo dictionary

print(posts["Posts"])

exit()

#Para accessar o contéudo do JSON Posts devemos utilizar o índice seguindo a estrutura do JSON. 
# Remova o comentário e a função exit() na linha 43 para imprimir no terminal 

print(posts["Posts"][0])

#Para acessar os atributos desse JSON devemos utilizar o index do atributo. 
#Isso equivalente a utilizar a sintaxe: posts["Posts"][0].title em javascript
# Remova o comentário e a função exit() na linha 43 para imprimir no terminal

print(posts["Posts"][0]['title'])


#Agora abra o arquivo api.py e termine de implementar o método post() para fazer requisições do tipo POST
#Para testar a sua função remova o exit() na linha 43 depois tente imprimir o valor de newPost na tela
createNewPost: int = api.post() 

