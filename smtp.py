from smtplib import SMTP_SSL, ssl
import re

class Email(SMTP_SSL):
   #Abaixo temos o construtor padrão para classes em Python, observe que:

   #1) A classe EMAIl esta herdando da classe SMTP_SSL. Isso é o equivalente ao "extends" em javascript
   #Exemplo em javascript: class Email extends SMTP_SSL

   #2) No Python a sintaxe para definir uma função é "def". Abaixo temos um "def" seguido de "__init__" o que indica
   #que estamos chamando uma função construtora para a classe

   #3) Utilizamos o __init__ para indiciar a a inicialização de um estado padrão do objeto a ser instaciando.

   #4) Além disso, no Python utiliziamos o "self" para apontar para o proprio objeto que estamos fazendo referência.
   #O SELF equivale ao THIS em outras linguagens
   #Exemplo: "self.host" seria o mesmo que "this.host" em PHP
   

    def __init__(self, email=None, password=None):
        self.host = "smtp.gmail.com"                        # Serviço SMTP desejado
        self.port = 465                                     # Porta SSL do serviço desejado           
        self.context = ssl.create_default_context()         # Criar um contexto SMTP
        self.email = email                                  # O Email do remetente
        self.password = password                            # A senha do remetente
        self.smtp = SMTP_SSL(self.host, self.port)          # Instanciando um objeto SMTP_SSL
        self.to_addrs = "List of Addresses"                 # Lista de Endereços (Destinatários)
        self.body = "Email Body"                            # Corpo do Email
        self.subject = "Subject"                            # Assunto do Email (Opcional)

    # Aqui temos as definições dos métodos que iremos criar para a classe. É importante dizer que:

    #1) O parametro self é opcional na definição do método. Para acessar os atributos da classe é necessário recebé-lo
    #como parâmetro
    
    #2)A tipagem dos parâmetros e do retorno do método também é opcional.
    #Exemplo: def sendEmail(self, email: str) -> boolean:
    #Nesse caso o método sendEmail precisa obrigatoriamente receber um email(string) e retornar um booleano

    #3) Também podemos definir um parametro como opcional, basta atribuir um valor padrão a ele
    #Exemplo: sendEmail(self, email : str = "Valor Inicial") -> boolean:
    #Nesse caso se for passado um valor a email na chamada do método o valor dele será "Valor Inicial"

    #4) Podemos também setar multiplos tipos para um parâmetro utilizando a biblioteca "typing"
    #Exemplo: def sendEmail(self, email: Union[str, list]):
    #Nesse caso email pode ser uma string ou uma lista

    #Método para enviar o email
    def sendEmail(self):
        smtp = self.smtp
        email = self.email
        password = self.password
        to_addrs = self.to_addrs
        msg = self.body
        
        try:
            # Login
            smtp.login(email, password) 
            print("Connected Successfully")
            
            # Sending the email
            if self.to_addrs == "List of Addresses":
                to_addrs = input("Digite o(s) email(s) do(s) destinatário(s): ") #List[str] of emails to be sent
            
            if self.body == "Email Body":
                msg = input("Digite o corpo do email: ") #The body of the email    
            
            smtp.sendmail(from_addr=email, to_addrs= to_addrs, msg=msg)

            print("Email enviado com sucesso")

            smtp.quit()
            smtp.close()

        except Exception as e:
            print(e)

    #Método para encerrar a conexão
    def closeConnection(self):
        smtp = self.smtp
        smtp.quit()
        smtp.close()

    
    #Getters and Setters
    def getEmail(self):
        return self.email
    
    def setEmail(self, email: str, password):
        self.email = email
        self.password = password

    def setAddresses(self, to_addrs: list or str):
        if type(to_addrs) == str:
            matches: list = re.findall("@\w+?\.com", to_addrs)
            if len(matches) > 1:
                to_addrs = re.split(",\s?", to_addrs)
            
        self.to_addrs = to_addrs
    
    def setMessage(self, msg):
        self.body = 'Subject: {}\n\n{}'.format(self.subject, msg)
    
    def setSubject(self, subject):
        self.subject = subject
        self.body = 'Subject: {}\n\n{}'.format(self.subject, self.body)

    def setEmailInfo(self, to_addrs=None, subject=None, message=None):
        if to_addrs != None:
            self.setAddresses(to_addrs)
        if subject != None:
            self.setSubject(subject)
        if message != None:
            self.setMessage(message)


